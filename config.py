import time

start_time = time.time()
max_user_threads = 1
max_auto_threads = 1

server = 'irc.server.com'
port = 6667
serverpw = ''

nick = 'casulbot'
nick_retries = ['casulbot2','casulbot3']

real_name = 'Casul IRC Bot'
#task_timeout = 60

#
# specify list of users which can run commands 
# restricted to mods-only (module checks engine.get_mods())
#
mods = [
    'Abraxas'
]

#
# specify channels for bot to join upon server connect
#
channels = [
    '#channel'
]

# TODO: figure out the purpose of this ...
connected_to = [] # always leave blank!

#
# upon joining a new channel, display welcome message?
#
show_welcome_messages = False

#
# re-connect parameters
#
connection_reconnect = True
connection_retry_interval = 10  # every 10 seconds, retry
connection_retry_count = 10     # if set, will only retry x times

#
# special bot configurations
#
shared_module_storage_dir = 'module_data'
command_lead = '!'
assigned_nick = ''

# TODO: this
public_antispam_interval = 10 # 10 seconds (at least) required between all bot requests (don't apply to mods)
private_antispam_interval = 1 # 1 second (at least) required between all bot requests (don't apply to mods)
