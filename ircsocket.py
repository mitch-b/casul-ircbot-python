# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

import sys
import time
from socket import *
import config


class IRCSocket():
  sock = socket()

  def __init__(self):
    pass

  def connect(self):
    addr = (config.server, config.port)
    print 'Initializing socket ...'
    self.sock = socket( AF_INET, SOCK_STREAM )

    try:
      print 'Connecting to {0}:{1}...'.format(addr[0], addr[1])
      self.sock.connect ( addr )
    except Exception as e:
      print 'Exception at', time.ctime()
      err = '[irc.connect FAILED] {0}'.format(e)
      raise Exception(err)

    if config.serverpw:
      print 'Sending PASS ...'
      self.PASS(config.serverpw)

    self.REGISTER(config.nick)

  def PONG(self, key):
    cmd = 'PONG {0}\r\n'.format(key)
    self._send(cmd)

  def PASS(self, pw):
    cmd = 'PASS {0}\r\n'.format(pw)
    self._send(cmd)

  def NICK(self, n):
    print "Sending NICK '{0}'...".format(n)
    cmd = 'NICK {0}\r\n'.format(n)
    self._send(cmd)

  def USER (self, userName, hostName, serverName, realName):
    print "Sending USER '{0}'...".format(userName)
    cmd = 'USER {0} {1} {2} :{3}\r\n'.format(userName, hostName, serverName, realName)
    self._send(cmd)

  def JOIN(self, chan):
    cmd = 'JOIN {0}\r\n'.format(chan)
    self._send(cmd)

  def PART(self, chan):
    cmd = 'PART {0}\r\n'.format(chan)
    self._send(cmd)

  def PRIVMSG(self, to, msg):
    if to is None:
      print "ERROR: No recipient of PRIVMSG. Specify comma separated recipients to complete command."
      return
    cmd = 'PRIVMSG {0} :{1}\r\n'.format(to, msg)
    self._send(cmd)

  def TOPIC(self, chan, topic):
    cmd = 'TOPIC {0} :{1}\r\n'.format(chan, topic)
    self._send(cmd)

  def CHAN_BROADCAST(self, msg):
    '''Send PRIVMSG to all connected channels'''
    self.PRIVMSG(','.join(config.channels), msg)

  def REGISTER(self, nick):
    self.NICK(nick)
    self.USER(nick, nick, config.server, config.real_name)

  def WHO(self, chan):
    cmd = 'WHO {0}\r\n'.format(chan)
    self._send(cmd)

  def LISTEN(self):
    buff = ''
    while True:
      try:
        buff += self.sock.recv(4096)
      except Exception as e:
        print 'Exception at', time.ctime()
        print '[irc.recv FAILED]', e


      if buff == '':
        print 'Connection dropped', time.ctime()
        print '[irc.recv FAILED]'
        raise Exception('Connection dropped')

      if buff.find('\n') == -1: # not at end of input
        continue
      command = buff[0 : buff.find('\n')]
      # buff = buff[buff.find('\n')+1 : ]

      #print '<< ', command
      return command, buff
      buff = ''


  def _send(self, cmd):
    try:
      #print '>> ', cmd
      self.sock.send(cmd)
    except Exception as e:
      print 'Exception at', time.ctime()
      print '[irc.send FAILED]', e
      print 'data: ', cmd
