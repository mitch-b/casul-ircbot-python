#!/usr/bin/env python

# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

import os
import sys
import time
import config
import ircparser
import ircsocket
import functions

from threading import Thread
from threading import Timer
import Queue


from axel import Event
import pkgutil
import casul_cmds
import casul_tasks


"""Casul IRC Bot v1

Uses Job Scheduler to enqueue tasks based on priority.
Might be able to switch to FIFO, but this is cool for now.

Will dynamically read in eligible Python modules at startup found
in casul_cmds/ and casul_tasks/.

This bot is a work-in-progress to be totally thread-safe.
"""
class CasulIRC:
  handlers = dict()
  autohandlers = dict()
  channel_users = dict()
  queue = Queue.PriorityQueue()
  auto_queue = Queue.PriorityQueue()
  connected = False
  irc = ircsocket.IRCSocket()


  def __init__(self, **kwargs):
    # First, clear terminal screen
    os.system('cls' if os.name == 'nt' else 'clear')
    print """
     casul IRC Bot
     > mitch-b
     > https://bitbucket.org/mitch-b/casul-ircbot-python
     > 1.2.0-beta
    """
    self.wire_events() # prepare IRC bot events
    self.setup()

  def run(self):
    try:
      self.loop()
    except Exception as ex:
      print ex
      config.connected_to = [] # TODO: manage #channel <--> is_connected relationship
      if config.connection_reconnect:
        config.retry_count = 1
        while self.connected == False:
          time.sleep(float(config.connection_retry_interval))
          if config.connection_retry_count > 0 and \
             config.retry_count > config.connection_retry_count:
            print 'Retry attempts failed.'
            sys.exit(1)
          try:
            self.loop()
          except Exception as e:
            print e
          config.retry_count += 1

  def loop(self):
    # attempt to connect to IRC server in config.py
    self.irc.connect()
    # run bot by listening on socket
    self.listen()

  def wire_events(self):
    # onConnected
    # onMessage
    # onError
    # onDisconnected
    # onModeChanged
    pass

  def setup(self):
    if config is None:
      print 'Missing config settings! Stopping execution.'
      sys.exit(1)
    self.load_modules('user', self.handlers)
    self.load_modules('task', self.autohandlers)

  def load_modules(self, modtype, handlers):
    if modtype is None:
      return
    pkgpath = ''
    package = ''
    if modtype == 'user':
      pkgpath = os.path.dirname(casul_cmds.__file__)
      package = 'casul_cmds'
    elif modtype == 'task':
      pkgpath = os.path.dirname(casul_tasks.__file__)
      package = 'casul_tasks'
    else:
      return
    print 'Loading {0} modules:'.format(modtype)
    for cmd in [name for _, name, _ in pkgutil.iter_modules([pkgpath])]:
      info = ''
      e = Event(self)
      try:
        module = '{0}.{1}'.format(package, cmd)
        module = __import__(module, globals(), locals(), [cmd])
      except ImportError as e:
        print '- IMPORT FAILED [{0}]: Ensure all import statements are installed and added to requirements.txt'
        continue
      if module:

        try:
          e += getattr(module, 'run')
        except AttributeError:
          print "- IMPORT FAILED [{0}]: Missing callable 'run' method.".format(cmd)
          continue

        try:
          getattr(module, 'init')(self) # TODO: pass a bundle to initialize modules
        except AttributeError:
          pass # no big deal if no initialization step provided
        except TypeError as te:
          print 'TypeError: ', te
          info = ' (init failed: bad signature)'
        except Exception as e:
          # catch-all exception
          print "- IMPORT FAILED [{0}]: {1}".format(cmd, e)
          continue
        modulecmds = None
        try:
          modulecmds = module.cmds
        except AttributeError:
          pass # no cmds specified
        if modulecmds:
          for handle in modulecmds:
            if handle in handlers:
              print "? IMPORT WARNING [{0}]: overwriting handler for {1}".format(cmd, handle)
            handlers[handle] = e
        else:
          handlers[cmd] = e

        print '+ {0}{1}'.format(cmd, info)

        if modtype == 'task':
          self.schedule_autotask((0,e,module.interval))

    print ''


  def auto_process(self):
    '''Daemon thread to monitor and execute auto-task PriorityQueue as items
    arrive. '''
    while True:
      if not self.connected:
        time.sleep(10)
        continue
      method = None
      interval = 0
      try:
        task = self.auto_queue.get(True, 2)
        interval = task[2]
        callable_method = task[1]
      except Queue.Empty as empty:
        continue
      # run auto task!
      callable_method()
      # schedule to run again at interval declared in module
      timer = Timer(float(interval), self.schedule_autotask, (task,))
      timer.start()

  def process(self):
    '''Daemon thread to monitor and execute PriorityQueue as items
    arrive. '''
    while True:
      try:
        msg = self.queue.get(True, 2)[1]
      except Queue.Empty as empty:
        continue
      if msg['action'] == 'PING': # PONG
        self.irc.PONG(msg['arguments'])
      elif msg['action'] == '001' or msg['action'] == 'JOIN': # WELCOME
        self.connected = True
        config.retry_count = 0
        # use list(set()) to remove duplicates
        config.connected_to = list(set(config.connected_to + msg['arguments'].split(',')))
        print 'Channels: ', str(config.connected_to)
        self.irc.JOIN(msg['arguments'])
        if config.show_welcome_messages:
          self.announce_modules()
      elif msg['action'] == 'PART':
        for chan in msg['arguments'].split(','):
          config.connected_to.remove(chan)
        print 'Channels: ', str(config.connected_to)
        self.irc.PART(msg['arguments'])
      elif msg['action'] == 'PRIVMSG':
        response = ''
        if msg['command'] in self.handlers:
          response = self.handlers[msg['command']](msg)
          if response and response[0] and response[0][1]:
            cmd_response = response[0][1]
            if isinstance(cmd_response, dict):
              self.queue.put((1, cmd_response)) # a task is creating another task!
            else:
              self.send_message(msg['to'], cmd_response)
        else:
          pass # did nothing with this queued task
      self.queue.task_done()


  def schedule_autotask(self, task):
    self.auto_queue.put(task)

  def send_message(self, to, data):
    '''Will send PRIVMSG for either string or list of string input'''
    # may be candidate for ircsocket function
    if not isinstance(data, (list)):
      self.irc.PRIVMSG(to, data)
    else:
      for d in data:
        self.irc.PRIVMSG(to, d)

  def get_mods(self):
    return config.mods

  def get_file_path(self, file):
    '''Called by submodules to return filepath to requested file'''
    return '{0}/{1}'.format(self.get_shared_module_dir(), file)

  def get_shared_module_dir(self):
    return config.shared_module_storage_dir

  def listen(self):
    while True:
      msg = dict()

      # block thread while waiting to receive input from socket
      try :
        command, buff = self.irc.LISTEN()
      except Exception as e:
        print 'Error during listen(): ', e
        self.connected = False
        break

      if command == '':
        continue

      try:
        msg = ircparser.parse_message(command)
        #print '<< ', msg
      except Exception as e:
        print 'Exception at', time.ctime()
        print '[ircparser.parse_message FAILED]', e
        print 'failed with string: "{0}"'.format(command)

      if not msg or not msg['action']:
        continue

      if msg['action'] == 'PRIVMSG':
        spam = functions.antispam(self, msg['sender'], msg)
        if not spam:
          self.queue.put((1, msg))


      elif msg['action'] == 'PING':
        print '{0} - PING/PONG'.format(time.ctime())
        self.queue.put((0, msg))


      elif msg['action'] == '001':  # Welcome message.
        config.assigned_nick = msg['action_args'][0]
        print 'Connected!'
        chans = ','.join(config.channels)
        print 'Sending JOIN [{0}] ...'.format(chans)
        msg['arguments'] = chans
        self.queue.put((0, msg))

      elif msg['action'] == '433': # nick already in use
        print "[!] nick already in use."
        if len(config.nick_retries) > 0:
          n = config.nick_retries.pop(0)
          self.irc.REGISTER(n)
        else:
          print '[!] All configured nicks in use. Please change in config.'
          sys.exit(2)

    raise Exception('Disconnected')


  def announce_modules(self):
    msg = dict()
    response = []
    response.append("Try '{0}list' to find supported commands".format(config.command_lead))
    response.append("Try '{0}help cmd' to find usage tips".format(config.command_lead))
    response.append("Use '/msg {0} cmd' to chat privately".format(config.assigned_nick))
    to = ','.join(config.channels)
    self.send_message(to, response)
    return



if __name__ == '__main__':
  bot = CasulIRC()

  # set up worker threads (will process all incoming IRC messages)
  print 'Spawning {0} thread(s) to process messages'.format(int(config.max_user_threads))
  for i in range(int(config.max_user_threads)):
    t = Thread(target=bot.process)
    t.daemon = True
    t.start()

  print 'Spawning {0} thread(s) to run automated tasks'.format(int(config.max_auto_threads))
  for i in range(int(config.max_auto_threads)):
    t = Thread(target=bot.auto_process)
    t.daemon = True
    t.start()

  try:
    bot.run()
  except KeyboardInterrupt:
    print '\nClosing...'
