# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

"""Mod Commands IRC Module
"""

cmds = ['join', 'leave']

import config

def join(sender, components):
  chan = components['commandargs']
  msg = { 'action': 'JOIN', 'arguments': chan }
  return msg

def leave(sender, components):
  chan = components['commandargs']
  msg = { 'action': 'PART', 'arguments': chan }
  return msg


def is_mod(user):
  return user in config.mods

def run(sender, components=None):
  if not is_mod(components['sender']):
    print 'non-mod user attempted to run mod command = {0} - {1}'.format(components['sender'], components['command'])
    return

  if components['command'] == 'join':
    return join(sender, components)
  elif components['command'] == 'leave':
    return leave(sender, components)
  else:
    print 'something went wrong in mod command module'

def help(components=None):
  submodule = components['arguments'].split(' ')[1]
  if submodule == 'join':
    return [
      'Usage: {0}join'.format(config.command_lead),
      'Action: bot will join new channel',
      'Limitations: command must be run by mod'
    ]
  else:
    return 'No help provided for {0}'.format(submodule)