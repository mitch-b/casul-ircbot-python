# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Points IRC Module

Read arbitrary internet points from sqlite table
"""

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

import re
import sqlite3
import functions
from rematcher import RegexMatcher

cmds = ['point', 'points'] # !point, !points

POINT_MATCH = '([A-Za-z_\-\[\]\\^{}|`][A-Za-z0-9_\-\[\]\\^{}|`]*)\s?(\d+)?'

POINTS_TABLE = 'user_points'
POINTS_TABLE_CREATE = '''
  CREATE TABLE user_points (
    SENDER TEXT,
    RECIPIENT TEXT,
    POINTS INTEGER
  );'''
SUM_QUERY = "SELECT RECIPIENT, SUM(POINTS) FROM user_points GROUP BY RECIPIENT ORDER BY SUM(POINTS) DESC"

def init(sender, components=None):
  try:
    con = sqlite3.connect(sender.get_file_path('points.db'), isolation_level=None)
    cursor = con.cursor()
    cursor.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{0}';".format(POINTS_TABLE))
    data = cursor.fetchone()[0]
    if data == 0:
      cursor.execute(POINTS_TABLE_CREATE)
  except sqlite3.Error as e:
    raise Exception('failed to load points.db: {0}'.format(e))
  except Exception as ex:
    print 'uncaught exception: ', ex
  finally:
    cursor.close()
    con.close()


def insert_points(sender, from_user, to_user, points):
  data = (from_user, to_user, points)
  try:
    con = sqlite3.connect(sender.get_file_path('points.db'), isolation_level=None)
    cursor = con.cursor()
    cursor.execute("INSERT INTO {0} VALUES (?, ?, ?)".format(POINTS_TABLE), data)
  except sqlite3.Error as e:
    print '>> Failed to load points.db: {0}'.format(e)
    raise Exception('Mod, please check log on server for details on why assigning points failed.')
  finally:
    cursor.close()
    con.close()


def read_points(sender, top):
  response = ['Point Leaderboard']
  sql = '{0} LIMIT {1}'.format(SUM_QUERY, top)
  try:
    con = sqlite3.connect(sender.get_file_path('points.db'))
    cursor = con.cursor()
    cursor.execute(sql)
    for row in cursor:
      response.append("{0}: {1} points".format(row[0], row[1]))
  except sqlite3.Error as e:
    print '>> Failed to load points.db: {0}'.format(e)
    raise Exception('Mod, please check log on server for details on why reading points failed.')
  except Exception as ex:
    print '>> Uncaught exception: {0}'.format(ex)
    raise ex
  finally:
    cursor.close()
    con.close()
  return response

def run_points(sender, components=None):
  count = 10
  if components['commandargs']:
    try:
      count = components['commandargs']
    except ValueError:
      pass
  return read_points(sender, count)

def run_point(sender, components=None):
  user = ''
  points = 1
  args = components['commandargs']
  from_user = components['sender']

  if from_user not in sender.get_mods():
    print 'user issuing command not a mod'
    return help(components)
  if args is None:
    print 'point called without another argument'
    return help(components)

  m = RegexMatcher(POINT_MATCH)
  if m.match(args):
    if m.group(1):
      user = m.group(1).strip().lower()
    if m.group(2):
      try:
        points = m.group(2).strip()
      except ValueError:
        pass
    if user == from_user.lower():
      print 'user trying to give point to self'
      return help(components)
  else:
    print 'point data didnt match regex'
    return help(components)

  insert_points(sender, from_user, user, points)

def run(sender, components=None):
  submodule = components['command']
  if submodule == 'point':
    return run_point(sender, components)
  elif submodule == 'points':
    return run_points(sender, components)


def help(components=None):
  submodule = components['arguments'].split(' ')[1]
  if submodule == 'points':
    return [
      'Usage: !points 10',
      "Action: Will return top 10 users' Internet points",
    ]
  elif submodule == 'point':
    return [
      'Usage: !point user',
      'Usage: !point user 5',
      "Action: Add 1 point (default) or given points to user's Internet point collection",
      "Limitations: Cannot add points to self",
      "Note: can only be executed by mods"
    ]
