# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

"""HackerNews Bot

Display top news story from third party HackerNews API
"""

import config
import requests
import inflection
import json
from decimal import *

max_articles = 3

cmds = ['hn'] # !hn

def run(sender, components):
  response = []
  count = 1
  if components['commandargs']:
    try:
      count = components['commandargs']
      if Decimal(count) > Decimal(max_articles):
        count = max_articles
    except ValueError:
      # not provided an integer value
      return 'Not sure what to do with this information...'

  response.append("HackerNews Top {0} {1}".format(count, inflection.pluralize('Article') if count > 1 else 'Article'))
  url = "http://hnify.herokuapp.com/get/top?limit={0}".format(count)

  try:
    result = requests.get(url)
    if result.status_code == 200:
      data = json.loads(result.text)
      index = 0
      for story in data['stories']:
        index += 1
        if index > max_articles: # sometimes the API will return more than requested
          break
        title = story['title']
        link = story['link']
        if title and link:
          article_title = title.encode('utf-8', 'replace')
          response.append('{0} - {1} [{2}]'.format(inflection.ordinalize(index), article_title, link))
        else:
          return "Sorry, I can't seem to function right today"
    else:
      print 'hn: Unable to read from {0}'.format(url)
  except Exception as e:
    print e
  return response

def help(components=None):
  return [
    'Usage: {0}hn 2'.format(config.command_lead),
    'Action: Will return top 2 stories from Hacker News.',
    'Limitations: max # of articles is {0}'.format(max_articles)
  ]