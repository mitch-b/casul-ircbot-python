# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Help IRC Module

Will respond to !help <cmd> commands. Loads module (if provided) and
calls that module's help() function to return a string (or list of strings).
"""

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

import sys
import config
import importlib

cmds = ['help', 'actions', 'cmds', 'list']

help_def = 'help'

def load_module_help(cmd, components=None):
  command_handlers = components['help_handler'].handlers
  for key in command_handlers.keys():
    # from sender.handlers['cmd'], we receive a tuple from our
    # Axel event, so let's find the module name of the first entry
    # ... which is our run() method!
    module_path = command_handlers[key][0].__module__

  try:
    module = importlib.import_module(module_path)
  except ImportError as e:
    return 'Failed import: {0}'.format(e)

  if not module:
    return 'Command not recognized.'

  try:
    return getattr(module, help_def)(components)
  except AttributeError as ae:
    return 'Unable to find help for {0}'.format(cmd)

def help(components=None):
  return [
    'Usage: {0}help <cmd>'.format(config.command_lead),
    'Example: {0}help uptime'.format(config.command_lead),
    'Action: Will display help (if available) for command.'
  ]

def run(sender, components=None):
  help_module = components['commandargs']
  cmd = components['command']
  
  try:
    components['help_handler'] = sender.handlers[help_module]
  except Exception as e:
    if help_module: # if user provided module which doesn't exist, quit
      print 'found no help handler for ', help_module
      return

  if help_module and help_module not in cmds:
      return load_module_help(help_module, components)
  elif help_module:
    cmdlist = sorted(sender.handlers)
    return ', '.join(cmdlist)
  else:
    return help(components)
