# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

"""Twitch Bot

Auto-post when configured user is streaming, and when finished.
"""

import requests
import json
import time
import ConfigParser

interval = 120

def init(sender):
  """ Run any initialization tasks here before IRC bot starts up """
  config = ConfigParser.ConfigParser()
  config.read(sender.get_file_path('twitch_stream.config.ini'))
  global streamer
  global endpoint
  global twitchchans
  global streaming
  streamer = config.get('TwitchStream', 'Stream')
  endpoint = config.get('TwitchStream', 'Endpoint')
  twitchchans = config.get('TwitchStream', 'TopicChannels')
  streaming = False

def run(sender, components=None):
  """ Method called by task scheduler """
  return twitch_stream(sender, components)

def set_topic(sender, topic):
  for chan in twitchchans.split(','):
    sender.irc.TOPIC(chan, topic)

def twitch_stream(sender, components):
  '''Read Twitch API on streamer to detect if streaming.

  If streamer is live, change channel topic (or send message?)
  '''
  global streaming
  url = '{0}/streams/{1}'.format(endpoint, streamer)
  try:
    result = requests.get(url)
    if result.status_code == 200:
      data = json.loads(result.text)
      if data is None:
        raise Exception('Failure to read JSON result from Twitch API')
      stream = data['stream']
      if stream and streaming == False:
        playing = stream[u'game']
        url = stream[u'channel'][u'url']
        set_topic(sender, "Abraxas is now streaming '{0}' : {1}".format(playing, url))
        streaming = True
      elif stream is None and streaming == True:
        set_topic(sender, "Stream offline for now! <3")
        streaming = False
    else:
      print 'Unable to read from {0}'.format(url)
  except Exception as e:
    streaming = False
    print e
