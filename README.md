Casul IRC Engine
============
Python implementation of IRC engine for bots

##About
This is an IRC Engine, meaning, it will perform all of the connections, user communication, and message handling events for your IRC bot. Your custom IRC bot commands will live as modules of this engine - callable by your endusers.

Therefore, code that is found within the engine itself should only reflect generic actions that will be relevant to most custom IRC bots. Keep this in mind when submitting Pull Requests.

##Cloning
```bash
$> git clone https://bitbucket.org/mitch-b/casul-ircbot-python
```

##Requirements
1. [Python 2.7](https://www.python.org/download/releases/2.7.8/)
1. pip Python Package Manager ([pip Installation Guide](https://pip.pypa.io/en/latest/installing.html))

##Developing for Casul IRC Engine
It is the end-goal that `engine.py` will be in such a state that it will not require any changes by developers, but simply adding new modules and modifying configuration settings will allow any features required. So you're probably interested in adding new functionality to this bot. The great news is, you only need to drop in 1 file, which is logically separated from the rest of the bot, and you're golden! Let's read more about how to accomplish this.

###Adding a new module
The beauty of this bot implementation is that it will automatically look for new modules at startup. The name of your file will be the keyword command associated with calling your code. So, if you have `!uptime` as your desired command, you'll add an `uptime.py` module in the `casul_cmds` folder. If you'd rather specify your command name, you can name your module whatever you'd like, and specify a `cmds` property of your module which will contain a list of all callable handles to your module.

####Module Requirements
For a standard user-requested module, you only need to maintain a function with signature:

```python
def run(sender, components=None):
  # your code here
  pass
```

If you need code to run as the bot is starting (before connecting to IRC), you can optionally include an init function. Similarly, if you want the `!help` module to expose any documentation you want to provide, you can optionally include that endpoint too.

```python
def init(sender, components=None):
  # your startup code here
  pass

def help(components=None):
  return ['Usage: !module <args>', 'Second line of help here.']
```

If you want your module to execute for multiple commands (rather than just keyword of file), you can add a parameter to your module listing all callable handles.

```python
# some_module.py

cmds = ['asdf', 'hello'] # this module is callable by !asdf, and !hello
```

So, putting it all together, a sample module will look like this:

```python
# curr_time.py

cmds = ['time'] # callable by !time, rather than default module name !curr_time

def init(sender, components=None):
  pass # I don't need to initialize anything. This method isn't even required.
def run(sender, components=None):
  return time.ctime()
def help(components=None):
  return ['Usage: !time', 'Action: Will return current time.']
```

####Task Requirements
If you want an automated task to run at scheduled intervals, you can create a python module in the `casul_tasks` folder to achieve this. Here, you are required to take one extra step of including your time interval (in seconds) as a part of your Python module. Here's a quick example.

```python
# newday.py

from datetime import datetime

interval = 30 # every 30 seconds run this package
day = 0

def init(sender, components=None):
  global day
  day = datetime.today().day

def run(sender, components=None):
  global day
  now = datetime.today().day
  if now != day:
  	day = now
  	sender.irc.CHAN_BROADCAST("It's a new day!")
```

##Configuring
You will want to modify `config.py` which stores runtime information such as which channels to connect to, hostname, nick, etc. If you are interested in the twitch_stream module, it has its own `.ini` file you'll want to look at.

##Running Casul IRC Engine
```bash
$> python engine.py
```

##Contributions
This engine has been modified with great ideas from paullik's [IRC-Bot](https://github.com/paullik/IRC-Bot), of which Casul IRC Engine has its extensibility & config modeled.

##License
Please see the [LICENSE](https://bitbucket.org/mitch-b/casul-ircbot-python/raw/master/LICENSE) file to read about the MIT License.