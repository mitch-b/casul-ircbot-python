# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "paullik"
__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"


from rematcher import RegexMatcher
import config

REGEX_CMD_MATCH = '^{0}(\w+)(.*)'.format(config.command_lead)
SEND_TO_MATCH = 'PRIVMSG\s(.*)?\s:'

def get_cmd_request(text):
  m = RegexMatcher(REGEX_CMD_MATCH)
  if m.match(text):
    return m.group(1).strip(), m.group(2).strip()
  else:
    return None, None

def get_sender(msg):
  '''Returns the user's nick (string) that sent the message'''
  return msg.split(":")[1].split('!')[0]

def send_to(command):
  '''Get the location where to send the message back

  This function returns a string containing all the protocol related
  information needed by the server to send the command back to the
  user/channel that sent it
  '''
  sendto = '' # can be a user's nick(/query) or a channel
  if -1 != command.find('PRIVMSG {0} :'.format(config.assigned_nick)):
    # the command comes from a query
    sendto = get_sender(command)
  else: # the command comes from a channel
    command = command[command.find('PRIVMSG #'):]
    command = command[command.find(' ')+1:]
    sendto = command[:command.find(' ')]
  return sendto

def parse_message(command):
  '''Returns an IRC command's components

  A dictionary will be filled by the data of the command, the command is as
  follows:
  :sender ACTION action_args :arguments

  sender(string) is the user who sent the command (only the user's nick)

  action(string) can be one of the following: PING, KICK, PRIVMSG, QUIT, etc.
  Check: http://www.irchelp.org/irchelp/rfc/chapter4.html#c4_2

  action_args(list of strings) depends on the ACTION, they are usually the
  channel or the user whom is the command for(see KICK, PRIVMSG, etc.), this
  will be a list and the items in the list will be the words that form the
  actual arguments

  arguments(string) depends on the ACTION

  command(string) first word starting with unique character lead (typically !)

  eg: the command ':foo!foo@domain.tld KICK #chan user :reason' will become:
      sender: 'foo'
      action: 'KICK'
      action_args: ['#chan', 'user']
      arguments: 'reason'
      command: !test
      to: '#chan'
  '''
  ''' @paullik : https://github.com/paullik/IRC-Bot '''

  inputcmd = command

  components = {
    'sender' : '',
    'action' : '',
    'action_args' : [],
    'arguments' : '',
  }

  if ':' == command[0]: # a user sent a command
    components['sender'] = get_sender(command)

    space_pos = command.find(' ') + 1
    command = command[space_pos:]
    space_pos = command.find(' ')

    components['action'] = command[:space_pos]

    command = command[space_pos + 1:]

    if ':' != command[0]: # action_args are present
      colon_pos = command.find(':')

      if -1 == colon_pos:
        colon_pos = len(command)+1

      components['action_args'] = command[:colon_pos-1].split()
      command = command[colon_pos:]

    if command and ':' == command[0]: # arguments are present
      components['arguments'] = command[1:]

  else: # the server sent a command
    space_pos = command.find(' ')
    components['action'] = command[:space_pos]
    components['arguments'] = command[space_pos+2:]

  components['arguments'] = components['arguments'].rstrip('\r')
  components['command'], components['commandargs'] = get_cmd_request(components['arguments'])
  components['to'] = send_to(inputcmd)

  return components
